﻿using System.Net;
using System.Security.Cryptography.X509Certificates;
/// <summary>
/// Esta es la clase Data, que contiene el día, fecha y año
/// </summary>
public class Data
{
    protected int dia;
    public int GetDia
    {
        get { return this.dia; }
    }

    public int SetDia
    {
        set { dia = value; }
    }
    protected int mes;
    public int GetMes
    {
        get { return this.mes; }
    }

    public int SetMes
    {
        set { mes = value; }
    }
    protected int any;

    public int GetAny
    {
        get { return this.any; }
    }

    public int SetAny
    {
        set { any = value; }
    }

    /// <summary>
    /// Valores de las fechas pasadas por parámetro
    /// </summary>
    public Data(int dia, int mes, int any)
    {
        this.dia = dia;
        this.mes = mes;
        this.any = any;
    }

    /// <summary>
    /// Valores predeterminados de la fecha
    /// </summary>
    public Data()
    {
        this.dia = 1;
        this.mes = 1;
        this.any = 1980;
    }
    /// <summary>
    /// Valores de las fechas creadas a partir de una copia
    /// </summary>
    public Data(Data date)
    {
        this.dia = date.dia;
        this.mes = date.mes;
        this.any = date.any;

    }
    /// <summary>
    /// Suma o resta un número a una fecha dependiendo de si es positivo o negativo
    /// </summary>
    public string sumarDades()
    {
        Console.WriteLine("Introduce un número y se lo sumaré o restaré a la fecha");
        int num = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("La suma de la data " + dia + "-" + mes + "-" + any + " y " + num + " es: ");

        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
        {
            if (num > 0)
            {
                for (int i = 0; i < num; i++)
                {
                    dia++;

                    if (dia >= 31)
                    {
                        dia = 0;
                        mes += 1;
                    }

                    if (mes > 12)
                    {
                        mes = 1;
                        any += 1;
                    }
                }
            }

            else if (num < 0)
            {
                for (int i = 0; i > num; i--)
                {
                    dia--;

                    if (dia <= 1)
                    {
                        dia = 31;
                        mes -= 1;
                    }

                    if (mes < 1)
                    {
                        mes = 12;
                        any -= 1;
                    }
                }

            }
        }

        else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
        {
            if (num > 0)
            {
                for (int i = 0; i < num; i++)
                {
                    dia++;

                    if (dia >= 30)
                    {
                        dia = 0;
                        mes += 1;
                    }

                    if (mes > 12)
                    {
                        mes = 1;
                        any += 1;
                    }
                }
            }

            else if (num < 0)
            {
                for (int i = 0; i > num; i--)
                {
                    dia--;

                    if (dia <= 1)
                    {
                        dia = 30;
                        mes -= 1;
                    }

                    if (mes < 1)
                    {
                        mes = 12;
                        any -= 1;
                    }
                }

            }
        }

        else if (mes == 2)
        {
            if (num > 0)
            {

                for (int i = 0; i < num; i++)
                {
                    dia++;

                    if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                    {
                        if (dia >= 29)
                        {
                            dia = 0;
                            mes += 1;
                        }

                        if (mes > 12)
                        {
                            mes = 1;
                            any += 1;
                        }
                    }

                    else
                    {
                        if (dia >= 28)
                        {
                            dia = 0;
                            mes += 1;
                        }

                        if (mes > 12)
                        {
                            mes = 1;
                            any += 1;
                        }
                    }
                }
            }

            else if (num < 0)
            {
                for (int i = 0; i > num; i--)
                {
                    dia--;

                    if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                    {
                        if (dia <= 1)
                        {
                            dia = 31;
                            mes -= 1;
                        }

                        if (mes < 1)
                        {
                            mes = 12;
                            any -= 1;
                        }
                    }
                }

            }
        }

        return dia + "-" + mes + "-" + any;
    }
    /// <summary>
    /// Compara dos fechas y dice si una es mayor, menor o igual que la otra
    /// </summary>
    public int compararDades(Data date1, Data date2)
    {
        if (date1.any > date2.any)
        {
            return 1;
        }

        else if (date1.any == date2.any)
        {
            if (date1.mes > date2.mes)
            {
                return 1;
            }

            else if (date1.mes == date2.mes)
            {
                if (date1.dia > date2.dia)
                {
                    return 1;
                }

                else if (date1.dia == date2.dia)
                {
                    return 3;
                }
            }
        }

        return 2;
    }
    /// <summary>
    /// Calcula cuantos días separan dos fechas
    /// </summary>
    public string diesQueSeparan(Data date1, Data date2)
    {
        int diferencia = 0;

        Console.Write("A " + date1.dia + "-" + date1.mes + "-" + date1.any + " y " + date2.dia + "-" + date2.mes + "-" + date2.any + " los separan ");

        date1.dia = pasarADias(date1);

        date2.dia = pasarADias(date2);

        if (compararDades(date1, date2) == 1 || compararDades(date1, date2) == 3)
        {
            diferencia = date1.dia - date2.dia;
        }

        else if (compararDades(date1, date2) == 2)
        {
            diferencia = date2.dia - date1.dia;
        }

        return diferencia + " dias";
    }
    /// <summary>
    /// Pasa años y meses a días
    /// </summary>
    public int pasarADias(Data date)
    {
        date.any *= 365;
        if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
        {
            date.mes *= 31;
        }

        else if( mes == 4 || mes == 6 || mes == 9 || mes == 11)
        {
            date.mes *= 31;
        }

        else if( mes == 2)
        {
            if(any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
            {
                date.mes *= 29;
            }

            else
            {
                date.mes *= 28;
            }
        }
        
        date.dia += date.any + date.mes;

        return date.dia;
    }
    /// <summary>
    /// Escribe el mensaje según lo que devuelve la función compararDades
    /// </summary>
    public string compararDadesMessage(Data dia1, Data dia2)
    {
        if (dia1.compararDades(dia1, dia2) == 1)
        {
            return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es major que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;
        }

        else if (dia1.compararDades(dia1, dia2) == 2)
        {
            return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es menor que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;

        }

        else if(dia1.compararDades(dia1, dia2) == 3)
        {
            return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es igual que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;

        }

        else
        {
            return "Holaaaa";
        }

    }

}
/// <summary>
/// Clase de pruebas para las fechas
/// </summary>
internal class ProvaDates
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el día: ");
        int dia = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el mes: ");
        int mes = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el any: ");
        int any = Convert.ToInt32(Console.ReadLine());

        ProvaDates pd = new ProvaDates();

        if (pd.isCorrectDate(dia, mes, any) == false)
        {
            dia = 1;
            mes = 1;
            any = 1980;
        }

        Data dia1 = new Data(dia, mes, any);
        Data dia2 = new Data(28, 03, 2023);

        Console.WriteLine("Sumar dades: ");
        Console.WriteLine(dia1.sumarDades());

        Console.WriteLine("Comparar dades: ");
        Console.WriteLine(dia1.compararDadesMessage(dia1, dia2));

        Console.WriteLine("Días que separan: ");
        Console.WriteLine(dia1.diesQueSeparan(dia1, dia2));
    }
    /// <summary>
    /// Comprueba si la fecha es válida
    /// </summary>
    public bool isCorrectDate(int dia, int mes, int any)
    {
        if (mes < 1 || mes > 12)
        { return false; }

        else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
        {
            if (dia > 31 && dia < 1)
            { return false; }

            else
            { return true; }
        }

        else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
        {
            if (dia > 30 && dia < 1)
            { return false; }

            else
            { return true; }
        }

        else if (mes == 2)
        {
            if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
            {
                if (dia > 29 && dia < 1)
                { return false; }

                else
                { return true; }
            }

            else
            {
                if (dia > 28 && dia < 1)
                { return false; }

                else
                { return true; }
            }
        }

        else
        { return false; }

    }
}